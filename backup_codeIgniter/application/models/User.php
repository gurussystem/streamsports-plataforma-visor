<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model
{

    public function login($user, $username, $pass)
    {
        if ($user === $username) {
            $this->load->model('securehash');
            $query = $this->db->get_where('users', array('user' => $user));
            if ($query->num_rows() == 1) {
                $user = $query->row();
                return $this->securehash->validate_hash($pass, $user->pass, $user->salt);
            } else
                return false;
        } else
            return false;
    }

    public function get($user)
    {
        $query = $this->db->get_where('users', array('user' => $user));
        if ($query->num_rows() == 1)
            return $query->row();
        else
            return false;
    }

    public function getVideos($userId)
    {
        $this->db->select('*');
        $this->db->from('grabaciones');
        $this->db->join('partidos', 'grabaciones.idPartido = partidos.idPartido');
        $this->db->where('partidos.idUsuario', $userId);
        return $this->db->get()->result();
    }

    public function getVideo($userId, $videoId)
    {
        $this->db->select('*');
        $this->db->from('grabaciones');
        $this->db->join('partidos', 'grabaciones.idPartido = partidos.idPartido');
        $this->db->where('partidos.idUsuario', $userId);
        $this->db->where('grabaciones.idGrabacion', $videoId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}
