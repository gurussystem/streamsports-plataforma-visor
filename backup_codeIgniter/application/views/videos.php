<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>StreamSports</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/semantic/dist/semantic.min.css') ?>">
    <script src="<?= base_url('assets/jquery-2.1.4.min.js') ?>"></script>
    <script src="<?= base_url('assets/semantic/dist/semantic.min.js') ?>"></script>
    <style>
        h1 {
            margin-top: 3em !important;
        }
        .video-thumbnail{
            position: relative;
        }
        .video-thumbnail img:hover{
            opacity: 0.7;
        }
        .video-thumbnail i{
            position: absolute;
            top: 50%;
            left:0;
            width: 100%;
            color:white;
            font-size:40px;
        }
    </style>
</head>
<body>
<div class="ui container">

    <h1>Vídeos <?= $subdomain ?></h1>


    <? $i = 0 ?>
    <? foreach ($videos as $video): ?>
        <? if ($i == 0): ?>
            <div class="ui three column grid">
        <? endif ?>
        <div class="column">
            <div class="ui card">
                <div class="content">
                    <div class="header"><?= $video->local ?> - <?= $video->visitante ?></div>
                    <div class="meta">
                        <span class="right floated time"><?= date('d/m/Y', strtotime($video->fechaIni)) ?></span>
                        <span class="category"><?= $video->localGol ?> - <?= $video->visitanteGol ?></span>
                    </div>
                    <a class="description video-thumbnail" href="<?=base_url('play/'.$video->idGrabacion)?>">
                        <img src="<?= base_url('/assets/img/game.jpg') ?>">
                        <i class="icon play"></i>
                    </a>
                </div>
                <div class="extra content">
                    <div class="right floated">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular facebook icon button">
                            <i class="facebook icon"></i>
                        </a>
                        <a href="https://twitter.com/home?status=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular twitter icon button">
                            <i class="twitter icon"></i>
                        </a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= base_url('play/'.$video->idGrabacion)?>&title=Partido&summary=<?= urlencode($video->local.' - '.$video->visitante)?>&source=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular linkedin icon button">
                            <i class="linkedin icon"></i>
                        </a>
                        <button class="ui circular google plus icon button">
                            <i class="google plus icon"></i>
                        </button>
                        <a href="mailto:?subject=<?= $video->local.' - '.$video->visitante?>&amp;body=<?= base_url('play/'.$video->idGrabacion)?>" class="ui circular icon button">
                            <i class="icon envelope"></i>
                        </a>
                        <!--<button class="circular ui icon button">
                            <i class="icon cloud download"></i>
                        </button>-->
                    </div>
                </div>
            </div>
        </div>
        <? if ($i == 2): ?>
            </div>
            <? $i = 0 ?>
        <? else: ?>
            <? $i++ ?>
        <? endif ?>
    <? endforeach ?>

</div>
</body>
</html>