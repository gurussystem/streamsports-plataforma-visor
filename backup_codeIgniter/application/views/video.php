<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?= $video->local ?> - <?= $video->visitante ?></title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/semantic/dist/semantic.min.css') ?>">
    <script src="<?= base_url('assets/jquery-2.1.4.min.js') ?>"></script>
    <script src="<?= base_url('assets/semantic/dist/semantic.min.js') ?>"></script>
    <style>
        h1 {
            margin-top: 3em !important;
        }

        .video-thumbnail {
            position: relative;
        }

        .video-thumbnail img:hover {
            opacity: 0.7;
        }

        .video-thumbnail i {
            position: absolute;
            top: 50%;
            left: 0;
            width: 100%;
            color: white;
            font-size: 40px;
        }
    </style>
</head>
<body>
<div class="ui container">

    <h1><?= $video->local ?> - <?= $video->visitante ?> (<?= $video->localGol ?> - <?= $video->visitanteGol ?>)</h1>

    <h3><?= date('d/m/Y', strtotime($video->fechaIni)) ?></h3>
    <a href="<?= base_url() ?>">Ver todos los vídeos de <?= $subdomain?></a>
    <div class="ui divider"></div>
    <div class="ui embed" data-url="<?= $video->archivo ?>">
    </div>
    <div class="ui divider"></div>
    <div class="ui right aligned container">

        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('play/' . $video->idGrabacion) ?>" class="ui circular facebook icon button">
            <i class="facebook icon"></i>
        </a>
        <a href="https://twitter.com/home?status=<?= base_url('play/' . $video->idGrabacion) ?>" class="ui circular twitter icon button">
            <i class="twitter icon"></i>
        </a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= base_url('play/' . $video->idGrabacion) ?>&title=Partido&summary=<?= urlencode($video->local . ' - ' . $video->visitante) ?>&source=<?= base_url('play/' . $video->idGrabacion) ?>" class="ui circular linkedin icon button">
            <i class="linkedin icon"></i>
        </a>
        <button class="ui circular google plus icon button">
            <i class="google plus icon"></i>
        </button>
        <a href="mailto:?subject=<?= $video->local . ' - ' . $video->visitante ?>&amp;body=<?= base_url('play/' . $video->idGrabacion) ?>" class="ui circular icon button">
            <i class="icon envelope"></i>
        </a>
        <? if ($this->session->has_userdata('logged')): ?>
            <a href="#" class="circular ui icon button">
                <i class="icon cloud download"></i>
            </a>
        <? else: ?>
            <a href="<?= base_url('login') ?>" class="circular ui icon button">
                <i class="icon cloud download"></i>
            </a>
        <? endif ?>
    </div>
</div>
<script>
    $('.ui.embed').embed();
</script>
</body>
</html>