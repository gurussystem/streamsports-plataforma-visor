<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Acceso</title>
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/semantic/dist/semantic.min.css') ?>">
    <script src="<?= base_url('assets/jquery-2.1.4.min.js') ?>"></script>
    <script src="<?= base_url('assets/semantic/dist/semantic.min.js') ?>"></script>
    <style>
        h1 {
            margin-top: 3em !important;
        }
    </style>
</head>
<body>
<div class="ui container">

    <h1>Acceso</h1>

    <? if (isset($error)):?>
        <div class="ui red message">Usuario o contraseña incorrectos</div>
    <?endif?>
    <form class="ui form" method="post">
        <div class="field">
            <label>Usuario</label>
            <input type="text" name="user" placeholder="Usuario">
        </div>
        <div class="field">
            <label>Contraseña</label>
            <input type="password" name="pass" placeholder="Contraseña">
        </div>
        <button class="ui button" type="submit">Entrar</button>
    </form>
</div>
</body>
</html>